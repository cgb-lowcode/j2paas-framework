/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo;

import cn.easyplatform.cfg.EngineConfiguration;
import cn.easyplatform.cfg.TaskExecuter;
import cn.easyplatform.entities.beans.ResourceBean;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.spi.listener.ApplicationListener;
import cn.easyplatform.services.SystemServiceId;
import cn.easyplatform.services.system.DataSourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractDemoTest {

    protected static DataSourceService ds1;

    protected static DataSourceService ds2;

    protected final static Logger log = LoggerFactory.getLogger(AbstractDemoTest.class);

    public static void connect() throws Exception {
        if (ds1 != null)
            return;
        EngineConfiguration engineConfiguration = new EngineConfiguration() {

            @Override
            protected void initEngineService() {

            }

            @Override
            public void start() {

            }

            @Override
            public TaskExecuter getTaskExecuter(CommandContext cc) {
                return null;
            }
        };
        System.setProperty("derby.system.home",
                "D:\\MyWork\\granite-5.0\\granite\\granite-core");
        ResourceBean bean = new ResourceBean();
        bean.setId(SystemServiceId.SYS_ENTITY_DS);
        bean.setName("demo 参数数据库");
        Properties properties = new Properties();
        properties.load(AbstractDemoTest.class
                .getResourceAsStream("jdbc1.properties"));
        Map<String, String> props = new HashMap<String, String>();
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            props.put(entry.getKey().toString(), entry.getValue().toString());
        }
        bean.setProps(props);
        ds1 = new DataSourceService(null, bean);
        ds1.setEngineConfiguration(engineConfiguration);
        ds1.start();

        bean = new ResourceBean();
        bean.setId("demo");
        bean.setName("demo 业务数据库");
        properties = new Properties();
        properties.load(AbstractDemoTest.class
                .getResourceAsStream("jdbc2.properties"));
        props.clear();
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            props.put(entry.getKey().toString(), entry.getValue().toString());
        }
        bean.setProps(props);
        ds2 = new DataSourceService(null, bean);
        ds2.setEngineConfiguration(engineConfiguration);
        ds2.start();
    }

    public static void close() {
        if (ds1 != null)
            ds1.stop();
        if (ds2 != null)
            ds2.stop();
    }
}
