/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.service;

import cn.easyplatform.EngineService;
import cn.easyplatform.engine.cmd.addon.*;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.request.SimpleTextRequestMessage;
import cn.easyplatform.spi.service.AddonService;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AddonServiceImpl extends EngineService implements AddonService {


    @Override
    public IResponseMessage<?> selectObject(SimpleRequestMessage req) {
        return commandExecutor.execute(new SelectObjectCmd(req));
    }

    @Override
    public IResponseMessage<?> selectList(SimpleRequestMessage req) {
        return commandExecutor.execute(new SelectListCmd(req));
    }


    @Override
    public IResponseMessage<?> selectOne(SimpleRequestMessage req) {
        return commandExecutor.execute(new SelectOneCmd(req));
    }

    @Override
    public IResponseMessage<?> update(SimpleRequestMessage req) {
        return commandExecutor.execute(new UpdateCmd(req));
    }

    @Override
    public IResponseMessage<?> commit(SimpleRequestMessage req) {
        return commandExecutor.execute(new CommitCmd(req));
    }

    @Override
    public IResponseMessage<?> searchEntity(SimpleRequestMessage req) {
        return commandExecutor.execute(new SearchEntityCmd(req));
    }

    @Override
    public IResponseMessage<?> getEntity(SimpleTextRequestMessage req) {
        return commandExecutor.execute(new GetEntityCmd(req));
    }

    @Override
    public IResponseMessage<?> getVariable(SimpleTextRequestMessage req) {
        return commandExecutor.execute(new GetVariableCmd(req));
    }

    @Override
    public IResponseMessage<?> getSpot(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetSpotCmd(req));
    }

    @Override
    public IResponseMessage<?> getForward(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetForwardCmd(req));
    }

    @Override
    public IResponseMessage<?> callback(SimpleRequestMessage req) {
        return commandExecutor.execute(new CallbackCmd(req));
    }
}
