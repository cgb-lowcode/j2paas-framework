/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime.jasper.ds;

import cn.easyplatform.EntityNotFoundException;
import cn.easyplatform.ScriptEvalExitException;
import cn.easyplatform.ScriptRuntimeException;
import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.engine.runtime.jasper.ds.RecordComparator.OrderField;
import cn.easyplatform.entities.beans.LogicBean;
import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.support.Closeable;
import cn.easyplatform.support.scripting.CompliableScriptEngine;
import cn.easyplatform.support.scripting.ScriptEngineFactory;
import cn.easyplatform.type.EntityType;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DatalistDataSource extends AbstractDataSource implements Closeable {

	private Iterator<RecordContext> itr;

	private CompliableScriptEngine engine;

	private CommandContext cc;

	public DatalistDataSource(CommandContext cc, RecordContext rc,
			ListContext lc, EventLogic onRecord, String listOrder) {
		this.cc = cc;
		if (lc.getRecords() != null) {
			if (!Strings.isBlank(listOrder) && lc.getRecords().size() > 1) {
				List<RecordContext> records = new ArrayList<RecordContext>(
						lc.getRecords());
				sort(listOrder, records);
				itr = records.iterator();
			} else
				itr = lc.getRecords().iterator();
			if (onRecord != null && itr.hasNext()) {
				String content = null;
				if (!Strings.isBlank(onRecord.getId())) {
					LogicBean lb = cc.getEntity(onRecord.getId());
					if (lb == null)
						throw new EntityNotFoundException(
								EntityType.LOGIC.getName(), onRecord.getId());
					content = lb.getContent().trim();
				} else if (!Strings.isBlank(onRecord.getContent()))
					content = onRecord.getContent().trim();
				if (!Strings.isBlank(content))
					engine = ScriptEngineFactory.createCompilableEngine(cc,
							content);
			}
		} else {
			List<RecordContext> records = Collections.emptyList();
			itr = records.iterator();
		}
	}

	private void sort(String listOrder, List<RecordContext> pcList) {
		String[] aof = listOrder.split("\\,");
		List<OrderField> tempList = new ArrayList<OrderField>();
		for (String field : aof) {
			field = field.trim();
			if (!field.equals("")) {
				boolean isDesc = false;
				if (field.indexOf(" ") > 0) {
					String s = StringUtils.substringAfter(field, " ").trim();
					field = StringUtils.substringBefore(field, " ").trim();
					isDesc = s.indexOf("desc") >= 0 || s.indexOf("DESC") >= 0;
				}
				OrderField of = new OrderField(field, isDesc);
				tempList.add(of);
			}
		}
		Collections.reverse(tempList);
		for (OrderField orderField : tempList)
			Collections.sort(pcList, new RecordComparator(orderField));
	}

	@Override
	public boolean next() throws JRException {
		boolean hasNext = itr.hasNext();
		if (hasNext) {
			current = itr.next();
			if (engine != null) {
				try {
					engine.eval(current);
				} catch (ScriptEvalExitException ex) {
					if (!current.getParameterAsBoolean("855")) {// 如果855不为真，则直接退出
						throw new ScriptRuntimeException(ex.getMessage(),
								cc.getMessage(ex.getMessage(), ex.getRecord()));
					} else
						return next();
				}
			}
		}
		return hasNext;
	}

	@Override
	public void close() {
		if (engine != null)
			engine.destroy();
		engine = null;
	}

}
