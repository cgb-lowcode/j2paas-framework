/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.h5;

import cn.easyplatform.dao.BizDao;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.dos.UserDo;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.h5.UserVo;
import cn.easyplatform.services.IProjectService;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.type.IResponseMessage;

import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetOrgUserCmd extends AbstractCommand<SimpleRequestMessage> {

	/**
	 * @param req
	 */
	public GetOrgUserCmd(SimpleRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		IProjectService ps = cc.getProjectService();
		BizDao dao = cc.getBizDao();
		List<FieldDo> params = new ArrayList<FieldDo>(1);
		params.add(new FieldDo(FieldType.VARCHAR, req.getBody()));
		List<FieldDo[]> result = dao.selectList(GetUserCmd.sql, params);
		List<UserVo> groups = new ArrayList<UserVo>();
		for (FieldDo[] fields : result) {
			String id = fields[0].getValue() == null ? "" : fields[0]
					.getValue().toString();
			String name = fields[1].getValue() == null ? "" : fields[1]
					.getValue().toString();
			UserVo uv = new UserVo(id, name);
			UserDo user = ps.getSessionManager().getUser(id);
			if (user != null)
				uv.setStatus(user.isOpenMsn() ? UserVo.STATUS_LINK
						: UserVo.STATUS_UNLINK);
			groups.add(uv);
		}
		return new SimpleResponseMessage(groups);
	}

}
