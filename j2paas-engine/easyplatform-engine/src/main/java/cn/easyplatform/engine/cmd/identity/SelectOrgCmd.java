/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.identity;

import cn.easyplatform.dos.UserDo;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.LoginResponseMessage;
import cn.easyplatform.messages.vos.AuthorizationVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.log.LogManager;
import cn.easyplatform.type.StateType;
import cn.easyplatform.util.IdentityUtils;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SelectOrgCmd extends AbstractCommand<SimpleRequestMessage> {

    /**
     * @param req
     */
    public SelectOrgCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(final CommandContext cc) {
        AuthorizationVo av = IdentityUtils.getUserAuthorization(cc, cc.getUser(), (String) req.getBody());
        LoginResponseMessage resp = new LoginResponseMessage(av);
        resp.setOrgId((String) req.getBody());
        if (resp.isSuccess()) {
            UserDo userDo = cc.getUser();
            userDo.setState(StateType.START);
            cc.setUser(userDo);
            cc.getSync().clear();
            LogManager.startUser(cc.getEngineConfiguration().getLogPath(), cc.getEnv().getId(), cc.getUser());
        }
        return resp;
    }

}
