/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.admin;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.entities.beans.ResourceBean;
import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.stream.StringOutputStream;
import cn.easyplatform.messages.request.admin.ResourceSaveRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.admin.ResourceVo;
import cn.easyplatform.services.IDataSource;
import cn.easyplatform.spi.extension.ApplicationService;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.EntityUtils;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SaveResourceCmd extends
        AbstractCommand<ResourceSaveRequestMessage> {

    /**
     * @param req
     */
    public SaveResourceCmd(ResourceSaveRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        ResourceVo vo = req.getBody();
        if (vo.getType().equals(EntityType.DATASOURCE.getName())) {
            ApplicationService service = cc.getEngineConfiguration().getService(vo.getId());
            EntityInfo entity = null;
            if (service != null) {
                IDataSource ds = (IDataSource) service;
                ResourceBean rb = ds.getEntity();
                rb.setName(vo.getName());
                rb.setProps(vo.getProps());
                StringBuilder sb = new StringBuilder();
                StringOutputStream os = new StringOutputStream(sb);
                TransformerFactory.newInstance().transformToXml(ds.getEntity(), os);
                entity = new EntityInfo();
                entity.setId(vo.getId());
                entity.setName(vo.getName());
                entity.setType(vo.getType());
                entity.setSubType(vo.getSubType());
                entity.setContent(sb.toString());
                sb = null;
            } else {
                entity = EntityUtils.vo2Entity(vo);
            }
            entity.setStatus('U');
            cc.getEntityDao().updateModel(entity);
        } else {
            EntityInfo entity = EntityUtils.vo2Entity(vo);
            entity.setUpdateUser(cc.getUser().getId());
            entity.setStatus('U');
            cc.getProjectService().getEntityHandler().update(entity);
        }
        return new SimpleResponseMessage();
    }

}
