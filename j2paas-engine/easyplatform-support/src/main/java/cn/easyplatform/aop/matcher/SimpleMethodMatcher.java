/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.aop.matcher;

import cn.easyplatform.aop.MethodMatcher;

import java.lang.reflect.Method;

public class SimpleMethodMatcher implements MethodMatcher {

	private Method m;

	public SimpleMethodMatcher(Method method) {
		this.m = method;
	}

	public boolean match(Method method) {
		if (m == method)
			return true;
		if (!m.getName().equals(method.getName()))
			return false;
		Class<?>[] parameterTypesMe = m.getParameterTypes();
		Class<?>[] parameterTypesOut = method.getParameterTypes();
		if (parameterTypesMe.length != parameterTypesOut.length)
			return false;
		for (int i = 0; i < parameterTypesMe.length; i++)
			if (!parameterTypesMe[i].isAssignableFrom(parameterTypesOut[i]))
				return false;
		return true;
	}

}
