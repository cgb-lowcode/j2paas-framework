/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EasyPlatformWithLabelKeyException extends EasyPlatformRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Object[] args;

	private int lineNo = -1;

	private String source;

	public EasyPlatformWithLabelKeyException(String resourceId, Object... args) {
		super(resourceId);
		this.args = args;
	}

	public EasyPlatformWithLabelKeyException(String resourceId, Throwable cause,
			Object... args) {
		super(resourceId, cause);
		this.args = args;
	}

	public Object[] getArgs() {
		return args;
	}

	public int getLineNo() {
		return lineNo;
	}

	public void setLineNo(int lineNo) {
		this.lineNo = lineNo;
	}

	public String getSource() {
		return source == null ? "" : source;
	}

	public void setSource(String source) {
		if (this.source != null)
			return;
		if (source == null)
			source = "";
		else if (source.length() > 30)
			source = source.substring(0, 30) + "...";
		this.source = source;
	}

}
