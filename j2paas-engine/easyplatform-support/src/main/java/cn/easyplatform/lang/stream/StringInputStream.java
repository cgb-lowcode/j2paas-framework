/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.stream;

import cn.easyplatform.lang.Encoding;
import cn.easyplatform.lang.Lang;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class StringInputStream extends ByteArrayInputStream {

	public StringInputStream(CharSequence s, Charset charset) {
		super(toBytes(s, charset));
	}

	public StringInputStream(CharSequence s) {
		super(toBytes(s, Encoding.CHARSET_UTF8));
	}

	protected static byte[] toBytes(CharSequence str, Charset charset) {
		if (str == null)
			return new byte[0];
		if (charset == null)
			charset = Encoding.CHARSET_UTF8;
		try {
			return str.toString().getBytes(charset.name());
		} catch (UnsupportedEncodingException e) {
			throw Lang.wrapThrow(e);
		}
	}
}
