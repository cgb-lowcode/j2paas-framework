/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.bpm.access;

import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import org.snaker.engine.access.transaction.TransactionInterceptor;
import org.snaker.engine.access.transaction.TransactionStatus;

import java.sql.Connection;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EasyPlatformTransactionInterceptor extends TransactionInterceptor{

	@Override
	public void initialize(Object accessObject) {
	}

	@Override
	protected TransactionStatus getTransaction() {
        try {
            boolean isNew = false;
            if(JdbcTransactions.get() == null) {
                JdbcTransactions.begin(Connection.TRANSACTION_REPEATABLE_READ);
                isNew = true;
            }
            return new TransactionStatus(JdbcTransactions.get(), isNew);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
	}

	@Override
	protected void commit(TransactionStatus status) {
        try {
        	JdbcTransactions.commit();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            try {
            	JdbcTransactions.close();
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
	}

	@Override
	protected void rollback(TransactionStatus status) {
        try {
        	JdbcTransactions.rollback();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            try {
            	JdbcTransactions.close();
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }		
	}

}
