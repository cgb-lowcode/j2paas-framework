/* Copyright 2013-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.snaker.engine.core;

import cn.easyplatform.contexts.Contexts;
import org.snaker.engine.Context;
import org.snaker.engine.SnakerEngine;

/**
 * 单实例的服务上下文 具体的上下文查找服务交给Context的实现类
 * 
 * @author yuqs
 * @since 1.5
 */
public abstract class ServiceContext {

	public static Context getContext() {
		return Contexts.get(Context.class);
	}

	public static void setContext(Context context) {
		Contexts.set(Context.class, context);
	}

	public static void setEngine(SnakerEngine engine) {
		if (Contexts.get(SnakerEngine.class) == null)
			Contexts.set(SnakerEngine.class, engine);
	}

	public static SnakerEngine getEngine() {
		return Contexts.get(SnakerEngine.class);
	}
}
