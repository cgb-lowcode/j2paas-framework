/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.table;

import cn.easyplatform.entities.helper.FieldTypeAdapter;
import cn.easyplatform.type.FieldType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = {"name", "type", "length", "decimal", "description",
        "isNotNull", "defaultValue", "realType", "optionType", "optionValue", "acc"})
@XmlAccessorType(XmlAccessType.NONE)
public class TableField implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6880062233089755790L;

    @XmlElement(required = true)
    private String name;

    @XmlJavaTypeAdapter(value = FieldTypeAdapter.class)
    @XmlElement(required = true)
    private FieldType type;

    @XmlElement
    private int length;

    @XmlElement
    private int decimal;

    @XmlElement(required = true)
    private String description;

    @XmlElement(name = "notnull")
    private Boolean isNotNull;

    @XmlElement(name = "default-value")
    private String defaultValue;

    @XmlElement(name = "real-type")
    private String realType;

    @XmlElement
    private String acc;

    @XmlElement(name = "option-type")
    private Integer optionType;

    @XmlElement(name = "option-value")
    private String optionValue;

    public Integer getOptionType() {
        return optionType;
    }

    public void setOptionType(Integer optionType) {
        this.optionType = optionType;
    }

    public String getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }

    public String getRealType() {
        return realType;
    }

    public void setRealType(String realType) {
        this.realType = realType;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public FieldType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(FieldType type) {
        this.type = type;
    }

    /**
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * @return the decimal
     */
    public int getDecimal() {
        return decimal;
    }

    /**
     * @param decimal the decimal to set
     */
    public void setDecimal(int decimal) {
        this.decimal = decimal;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return
     */
    public Boolean isNotNull() {
        return isNotNull;
    }

    /**
     * @param isNotNull
     */
    public void setNotNull(Boolean isNotNull) {
        this.isNotNull = isNotNull;
    }

    /**
     * @return the defaultValue
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * @param defaultValue the defaultValue to set
     */
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getAcc() {
        return acc;
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{").append(name).append(",").append(type).append(",")
                .append(description).append("}");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TableField))
            return obj.equals(name);
        else if (((TableField) obj).name != null)
            return ((TableField) obj).name.equals(name);
        else
            return name == null;
    }

}
