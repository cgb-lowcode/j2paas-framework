/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.service;

import cn.easyplatform.messages.request.SimpleTextRequestMessage;
import cn.easyplatform.messages.request.vfs.*;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface VfsService {

    /**
     * 获取文件内容
     * @param req
     * @return
     */
    IResponseMessage<?> read(ReadRequestMessage req);

    /**
     * 写文件
     * @param req
     * @return
     */
    IResponseMessage<?> write(WriteRequestMessage req);

    /**
     * 重命名
     * @param req
     * @return
     */
    IResponseMessage<?> rename(RenameRequestMessage req);

    /**
     * 复制
     * @param req
     * @return
     */
    IResponseMessage<?> copy(CopyRequestMessage req);

    /**
     * 显示文件夹内容
     * @param req
     * @return
     */
    IResponseMessage<?> dir(DirRequestMessage req);

    /**
     * 删除文件
     * @param req
     * @return
     */
    IResponseMessage<?> delete(SimpleTextRequestMessage req);

    /**
     * 移动文件到其它目录
     * @param req
     * @return
     */
    IResponseMessage<?> move(MoveRequestMessage req);

    /**
     * 创建文件夹
     * @param req
     * @return
     */
    IResponseMessage<?> mkdir(SimpleTextRequestMessage req);
}
