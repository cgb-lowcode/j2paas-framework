/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.extension;

import cn.easyplatform.type.IResponseMessage;

import javax.sql.DataSource;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ProjectContext {

    /**
     * 项目id
     *
     * @return
     */
    String getId();

    /**
     * 项目名称
     *
     * @return
     */
    String getName();

    /**
     * easyplatform工作空间
     *
     * @return
     */
    String getWorkspace();

    /**
     * 当前项目工作空间
     *
     * @return
     */
    String getWorkPath();

    /**
     * $app/WEB-INF/
     *
     * @return
     */
    String getHome();

    /**
     * 获取主数据库的连接
     *
     * @return
     */
    DataSource getDataSource();

    /**
     * 获取指定数据库id的连接
     *
     * @return
     */
    DataSource getDataSource(String dbId);

    /**
     * 获取在线用户数
     *
     * @return
     */
    int getUserCount();

    /**
     * 获取配置信息
     *
     * @param name
     * @return
     */
    Map<String, String> getConfig(String name);

    /**
     * 获取配置项信息
     *
     * @param name
     * @return
     */
    String getConfigValue(String name);

    /**
     * 发布消息
     *
     * @param topic
     * @param data
     * @param toUsers
     */
    void publish(String topic, Serializable data, String... toUsers);

    /**
     * 执行功能
     *
     * @param id     功能参数id
     * @param inputs 输入参数
     * @return 返回执行结果
     */
    IResponseMessage<?> executeTask(ApplicationService service, String id, Map<String, Object> inputs);

    /**
     * 异步执行功能或逻辑
     *
     * @param id     功能参数id
     * @param inputs 输入参数
     * @return 返回Future对象
     */
    Future<IResponseMessage<?>> asyncTask(ApplicationService service, String id, Map<String, Object> inputs);

    /**
     * 执行逻辑
     *
     * @param id     逻辑参数id
     * @param inputs 输入参数
     * @return 返回执行结果
     */
    void executeLogic(ApplicationService service, String id, Map<String, Object> inputs);

    /**
     * 异步执行逻辑
     *
     * @param id     逻辑参数id
     * @param inputs 输入参数
     */
    void asyncLogic(ApplicationService service, String id, Map<String, Object> inputs);
}
