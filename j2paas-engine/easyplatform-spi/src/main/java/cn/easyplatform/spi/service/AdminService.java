/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.service;

import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.request.admin.ResourceSaveRequestMessage;
import cn.easyplatform.messages.request.admin.ServiceRequestMessage;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface AdminService {

    /**
     * 首页信息
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getHome(SimpleRequestMessage req);

    /**
     * 获取所有服务
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getServices(SimpleRequestMessage req);

    /**
     * 获取服务信息
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getService(ServiceRequestMessage req);

    /**
     * 设置服务状态
     *
     * @param req
     * @return
     */
    IResponseMessage<?> setServiceState(ServiceRequestMessage req);

    /**
     * 获取对应参数对象
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getModel(SimpleRequestMessage req);

    /**
     * 获取数据
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getData(SimpleRequestMessage req);

    /**
     * 保存项目
     *
     * @param req
     * @return
     */
    IResponseMessage<?> saveProject(SimpleRequestMessage req);

    /**
     * 保存资源数据，例如数据连接池的定义
     *
     * @param req
     * @return
     */
    IResponseMessage<?> saveResource(ResourceSaveRequestMessage req);

    /**
     * 获取项目入口的登陆、主页模版，包括主页面、应用入口
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getConfig(SimpleRequestMessage req);

    /**
     * 初始化缓存
     *
     * @param req
     * @return
     */
    IResponseMessage<?> initCache(SimpleRequestMessage req);

    IResponseMessage<?> enableCache(SimpleRequestMessage req);

    IResponseMessage<?> getCacheList(SimpleRequestMessage req);

    IResponseMessage<?> resetCache(SimpleRequestMessage req);

    IResponseMessage<?> reloadCache(SimpleRequestMessage req);

    IResponseMessage<?> deleteCache(SimpleRequestMessage req);

    /**
     * 获取日志文件系统
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getLog(SimpleRequestMessage req);

    /**
     * 实时监控日志，包括用户、应用
     *
     * @param req
     * @return
     */
    IResponseMessage<?> console(SimpleRequestMessage req);


    /**
     * 数据管理，包括sys_serial_info、业务锁
     *
     * @param req
     * @return
     */
    IResponseMessage<?> table(SimpleRequestMessage req);

}
