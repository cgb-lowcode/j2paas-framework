/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.request;

import cn.easyplatform.messages.AbstractRequestMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DebugRequestMessage extends AbstractRequestMessage<Boolean> {

	public static final int NONE_TYPE = 0;
	
	public static final int DEBUG_TYPE = 4;
	
	public static final int LOG_TYPE = 5;
	
	private int type;

	/**
	 * @param body
	 */
	public DebugRequestMessage(Boolean body, int type) {
		super(body);
		this.type = type;
	}

	public int getType() {
		return type;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
