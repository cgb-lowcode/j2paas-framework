/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.datalist;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListEvalVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6779368132024706979L;

	private String targetId;
	
	private String sourceId;
	
	private String logic;
	
	private List<Object[]> targetKeys;
	
	private List<Object[]> sourceKeys;

	/**
	 * @param targetId
	 * @param logic
	 * @param targetKeys
	 */
	public ListEvalVo(String targetId, String logic, List<Object[]> targetKeys) {
		this.targetId = targetId;
		this.logic = logic;
		this.targetKeys = targetKeys;
	}

	/**
	 * @return the targetId
	 */
	public String getTargetId() {
		return targetId;
	}

	/**
	 * @return the sourceId
	 */
	public String getSourceId() {
		return sourceId;
	}

	/**
	 * @return the logic
	 */
	public String getLogic() {
		return logic;
	}

	/**
	 * @return the targetKeys
	 */
	public List<Object[]> getTargetKeys() {
		return targetKeys;
	}

	/**
	 * @return the sourceKeys
	 */
	public List<Object[]> getSourceKeys() {
		return sourceKeys;
	}

	/**
	 * @param sourceId the sourceId to set
	 */
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	/**
	 * @param sourceKeys the sourceKeys to set
	 */
	public void setSourceKeys(List<Object[]> sourceKeys) {
		this.sourceKeys = sourceKeys;
	}
	
	
}
