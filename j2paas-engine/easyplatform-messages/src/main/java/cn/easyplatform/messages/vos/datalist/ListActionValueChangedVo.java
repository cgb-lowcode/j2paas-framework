/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.datalist;

import cn.easyplatform.messages.vos.ActionValueChangedVo;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListActionValueChangedVo extends ActionValueChangedVo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

	private Object[] keys;

	/**
	 * @param entityId
	 * @param sourceName
	 * @param targetName
	 * @param value
	 * @param mapping
	 * @param id
	 * @param keys
	 */
	public ListActionValueChangedVo(String entityId, String sourceName,
			String targetName, Object value, String mapping, String id,
			Object[] keys) {
		super(entityId, sourceName, targetName, value, mapping);
		this.id = id;
		this.keys = keys;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the keys
	 */
	public Object[] getKeys() {
		return keys;
	}

}
