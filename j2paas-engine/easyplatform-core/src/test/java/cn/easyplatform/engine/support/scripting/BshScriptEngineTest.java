/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.support.scripting;

import cn.easyplatform.ScriptEvalException;
import cn.easyplatform.ScriptEvalExitException;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.support.scripting.ScriptEngine;
import cn.easyplatform.support.scripting.ScriptEngineFactory;
import cn.easyplatform.support.scripting.ScriptEntry;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BshScriptEngineTest extends AbstractScriptEngineTest {

	public void test1() {
		CommandContext cc = new CommandContext();
		String expression = Streams.readAndClose(Streams.utf8r(getClass()
				.getResourceAsStream("btest1.bsh")));
		ScriptEngine engine = null;
		try {
			RecordContext rc = new RecordContext(createRecord(),
					createSystemVariables(), createUserVariables());
			engine = ScriptEngineFactory.createEngine()
					.init(new ScriptEntry(cc, expression,
							new RecordContext[] { rc }));
			engine.eval(1);
		} catch (ScriptEvalException ex) {
			System.err.println(String.format("exception:%d,%s", ex.getLine(),
					ex.getMessage()));
		} catch (ScriptEvalExitException ex) {
			int line = ex.getLine();
			Object ns = engine.getNameSpace();
			engine = ScriptEngineFactory.createEngine().init(
					new ScriptEntry(cc, expression, ns));
			try {
				engine.eval(line + 1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			engine.destroy();
		}
	}
}
