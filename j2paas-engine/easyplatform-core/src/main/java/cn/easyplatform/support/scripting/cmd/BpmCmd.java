/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.scripting.cmd;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.i18n.I18N;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.support.scripting.AbstractScriptCmd;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BpmCmd extends AbstractScriptCmd {

    BpmCmd(MainCmd cr) {
        super(cr);
    }

    /**
     * 获取流程开始的功能id
     *
     * @param processId
     */
    public String getInstanceTask(String processId) {
        return scc.getCommandContext().getBpmEngine()
                .getInstanceTask(processId);
    }

    /**
     * 启动流程实例并执行
     *
     * @param processId
     */
    public void startAndExecute(String processId) {
        scc.getCommandContext().getBpmEngine()
                .startAndExecute(processId, scc.getTarget());
    }

    /**
     * 启动流程实例并执行
     *
     * @param processId
     * @param nodeName
     */
    public void startAndExecute(String processId, String nodeName) {
        scc.getCommandContext().getBpmEngine()
                .startAndExecute(processId, nodeName, scc.getTarget());
    }

    /**
     * 执行指定任务
     *
     * @param taskId
     */
    public void execute(String taskId) {
        if (Strings.isBlank(taskId))
            throw new EasyPlatformWithLabelKeyException(I18N.getLabel("bpm.exec.error",
                    "864"));
        scc.getCommandContext().getBpmEngine().execute(taskId, scc.getTarget());
    }

    /**
     * 根据实例id、创建人、抄送人创建抄送记录
     *
     * @param orderId
     * @param actorIds
     */
    public void createCCOrder(String orderId, String... actorIds) {
        scc.getCommandContext().getBpmEngine().createCCOrder(orderId, actorIds);
    }

    /**
     * 更新状态用于更新抄送记录为已经阅读
     *
     * @param orderId
     * @param actorIds
     */
    public void updateCCStatus(String orderId, String... actorIds) {
        scc.getCommandContext().getBpmEngine()
                .updateCCStatus(orderId, actorIds);
    }

    /**
     * 任务提取,一般发生在参与者为部门、角色等组的情况下，该组的某位成员提取任务后，不允许其它成员处理该任务
     *
     * @param taskId
     */
    public void take(String taskId) {
        if (Strings.isBlank(taskId))
            throw new EasyPlatformWithLabelKeyException(I18N.getLabel("bpm.exec.error",
                    "864"));
        scc.getCommandContext().getBpmEngine()
                .take(taskId, scc.getTarget().getParameterAsString("720"));
    }

    /**
     * 撤回指定的任务
     *
     * @param taskId 任务主键ID
     */
    public void withdraw(String taskId) {
        if (Strings.isBlank(taskId))
            throw new EasyPlatformWithLabelKeyException(I18N.getLabel("bpm.exec.error",
                    "864"));
        scc.getCommandContext().getBpmEngine()
                .withdraw(taskId, scc.getTarget().getParameterAsString("720"));
    }

    /**
     * 结束当前任务，跳转到指定的nodeName
     *
     * @param taskId
     * @param nodeName
     */
    public void executeAndJumpTask(String taskId, String nodeName) {
        if (Strings.isBlank(taskId))
            throw new EasyPlatformWithLabelKeyException(I18N.getLabel("bpm.exec.error",
                    "864"));
        scc.getCommandContext().getBpmEngine()
                .executeAndJumpTask(taskId, nodeName, scc.getTarget());
    }

    /**
     * 拒绝
     *
     * @param taskId
     */
    public void reject(String taskId) {
        if (Strings.isBlank(taskId))
            throw new EasyPlatformWithLabelKeyException(I18N.getLabel(
                    "bpm.exec.error", "864"));
        scc.getCommandContext().getBpmEngine()
                .reject(taskId, scc.getTarget());
    }

    /**
     * 指派给其它人处理
     *
     * @param taskId
     * @param rc
     * @param actorIds
     */
    public void resignTo(String taskId, String... actorIds) {
        if (Strings.isBlank(taskId))
            throw new EasyPlatformWithLabelKeyException(I18N.getLabel("bpm.exec.error",
                    "864"));
        scc.getCommandContext().getBpmEngine()
                .resignTo(taskId, scc.getTarget(), actorIds);
    }

    /**
     * 指派给其它人处理
     *
     * @param taskId
     * @param rc
     * @param actorIds
     */
    public void resignTo(String taskId, int performType, String... actorIds) {
        if (Strings.isBlank(taskId))
            throw new EasyPlatformWithLabelKeyException(I18N.getLabel("bpm.exec.error",
                    "864"));
        scc.getCommandContext().getBpmEngine()
                .resignTo(taskId, performType, scc.getTarget(), actorIds);
    }

    /**
     * 对指定的任务分配参与者。参与者可以为用户、部门、角色
     *
     * @param taskId
     * @param actorIds
     */
    public void addTaskActor(String taskId, String... actorIds) {
        if (Strings.isBlank(taskId))
            throw new EasyPlatformWithLabelKeyException(I18N.getLabel("bpm.exec.error",
                    "864"));
        scc.getCommandContext().getBpmEngine().addTaskActor(taskId, actorIds);
    }

    /**
     * 对指定的任务分配参与者。参与者可以为用户、部门、角色
     *
     * @param taskId
     * @param actorIds
     */
    public void addTaskActor(String taskId, int performType, String... actorIds) {
        if (Strings.isBlank(taskId))
            throw new EasyPlatformWithLabelKeyException(I18N.getLabel("bpm.exec.error",
                    "864"));
        scc.getCommandContext().getBpmEngine()
                .addTaskActor(taskId, performType, actorIds);
    }

    /**
     * 删除其中的参与者
     *
     * @param taskId
     * @param actorIds
     */
    public void removeTaskActor(String taskId, String... actorIds) {
        if (Strings.isBlank(taskId))
            throw new EasyPlatformWithLabelKeyException(I18N.getLabel("bpm.exec.error",
                    "864"));
        scc.getCommandContext().getBpmEngine()
                .removeTaskActor(taskId, actorIds);
    }

    /**
     * 终止流程
     *
     * @param orderId
     */
    public void terminate(String orderId) {
        scc.getCommandContext()
                .getBpmEngine()
                .terminate(orderId, scc.getTarget().getParameterAsString("720"));
    }

    /**
     * 获取流程所有的TaskNode，返回功能id、名称
     *
     * @return
     */
    public String[][] getNodes(String processId) {
        return scc.getCommandContext().getBpmEngine().getNodes(processId);
    }

    /**
     * 获取流程中某一个任务结点下一步所有的TaskNode，返回功能id、名称
     *
     * @return
     */
    public String[][] getNextNodes(String processId, String nodeName, String... params) {
        Map<String, Object> args = null;
        if (params.length > 0) {
            args = new HashMap<String, Object>();
            for (String field : params)
                args.put(field, scc.getTarget().getValue(field));
        }
        return scc.getCommandContext().getBpmEngine().getNextNodes(processId, nodeName, args);
    }

    /**
     * 获取流程中任务结点的参与者，返回参与人、角色、类型
     *
     * @return
     */
    public String[] getActors(String processId, String nodeName) {
        return scc.getCommandContext().getBpmEngine().getActors(processId, nodeName);
    }
}
