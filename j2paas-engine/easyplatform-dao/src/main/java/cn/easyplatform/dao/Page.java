/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dao;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Page {

    // 当前页
    private int pageNo = 1;
    // 每页记录数
    private int pageSize = 10;
    // 总记录数
    private int totalCount = 0;
    // 排序
    private String orderBy;
    // 是否需要获取总笔数
    private boolean isGetTotal = true;

    public Page(int pageSize) {
        this.pageSize = pageSize;
    }

    public Page(int pageNo, int pageSize) {
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }

    public Page(int pageNo, int pageSize, boolean isGetTotal) {
        this.pageSize = pageSize;
        this.pageNo = pageNo;
        this.isGetTotal = isGetTotal;
    }

    /**
     * 获得当前页的页号,默认为1.
     */
    public int getPageNo() {
        return pageNo;
    }

    /**
     * 设置当前页的页号,小于1时自动设置为1.
     */
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
        if (pageNo < 1) {
            this.pageNo = 1;
        }
    }

    /**
     * 获得每页记录数.
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * 获得总记录数, 默认值为0.
     */
    public int getTotalCount() {
        return totalCount < 0 ? 0 : totalCount;
    }

    /**
     * 设置总记录数.
     */
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public boolean isGetTotal() {
        return isGetTotal;
    }

    public void setGetTotal(boolean isGetTotal) {
        this.isGetTotal = isGetTotal;
    }

    /**
     * 根据pageSize与totalCount计算总页数, 默认值为-1.
     */
    public int getTotalPages() {
        if (totalCount < 0) {
            return 0;
        }
        int count = totalCount / pageSize;
        if (totalCount % pageSize > 0) {
            count++;
        }
        return count;
    }
}
