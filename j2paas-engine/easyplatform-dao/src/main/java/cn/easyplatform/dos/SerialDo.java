/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dos;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SerialDo {

	private long nextKey;

	private String keyName;

	private int incVal;

	/**
	 * @param keyName
	 * @param nextKey
	 * @param incVal
	 */
	public SerialDo(String keyName, long nextKey, int incVal) {
		this.keyName = keyName;
		this.incVal = incVal;
		this.nextKey = nextKey;
	}

	public long getNextKey() {
		return nextKey;
	}

	public void setNextKey(long nextKey) {
		this.nextKey = nextKey;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public int getIncVal() {
		return incVal;
	}

	public void setIncVal(int incVal) {
		this.incVal = incVal;
	}
}
