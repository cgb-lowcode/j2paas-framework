{
    title: {
        text: '对数轴示例',
        left: 'center'
    },
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c}'
    },
    legend: {
        left: 'left',
        data: ['2的指数', '3的指数']
    },
    xAxis: {
        type: 'category',
        name: 'x',
        splitLine: {show: false},
        data: 'data'
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    yAxis: {
        type: 'log',
        name: 'y',
        minorTick: {
            show: true
        },
        minorSplitLine: {
            show: true
        }
    },
    series: [
        {
            name: '3的指数',
            type: 'line',
            data: 'data'
        },
        {
            name: '2的指数',
            type: 'line',
            data: 'data'
        },
        {
            name: '1/2的指数',
            type: 'line',
            data: 'data'
        }
    ]
}