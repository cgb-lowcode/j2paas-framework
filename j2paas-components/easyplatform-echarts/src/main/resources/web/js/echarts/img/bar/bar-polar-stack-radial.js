﻿{
    angleAxis: {
        type: 'category',
        data: 'data'
    },
    radiusAxis: {
    },
    polar: {
    },
    series: [{
        type: 'bar',
        data: 'data',
        coordinateSystem: 'polar',
        name: 'A',
        stack: 'a'
    }, {
        type: 'bar',
        data: 'data',
        coordinateSystem: 'polar',
        name: 'B',
        stack: 'a'
    }, {
        type: 'bar',
        data: 'data',
        coordinateSystem: 'polar',
        name: 'C',
        stack: 'a'
    }],
    legend: {
        show: true,
        data: ['A', 'B', 'C']
    }
}