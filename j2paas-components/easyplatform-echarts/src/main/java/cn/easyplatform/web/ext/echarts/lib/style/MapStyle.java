/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MapStyle implements Serializable {

    private String featureType;
    private String elementType;
    private Stylers stylers;

    public MapStyle(String featureType, String elementType) {
        this.featureType = featureType;
        this.elementType = elementType;
    }

    public String featureType() {
        return featureType;
    }

    public MapStyle featureType(String featureType) {
        this.featureType = featureType;
        return this;
    }

    public String elementType() {
        return elementType;
    }

    public MapStyle elementType(String elementType) {
        this.elementType = elementType;
        return this;
    }

    public Stylers stylers() {
        if (stylers == null)
            stylers = new Stylers();
        return stylers;
    }

    public MapStyle color(String color) {
        if (stylers == null)
            this.stylers = new Stylers();
        this.stylers.color(color);
        return this;
    }

    public MapStyle visibility(String visibility) {
        if (stylers == null)
            this.stylers = new Stylers();
        this.stylers.visibility(visibility);
        return this;
    }

    public String getFeatureType() {
        return featureType;
    }

    public void setFeatureType(String featureType) {
        this.featureType = featureType;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public Stylers getStylers() {
        return stylers;
    }

    public void setStylers(Stylers stylers) {
        this.stylers = stylers;
    }

    public MapStyle lightness(Object color) {
        if (stylers == null)
            this.stylers = new Stylers();
        this.stylers.lightness(color);
        return this;
    }

    public static class Stylers {
        private String color;
        private String visibility;
        private Object lightness;

        public String color() {
            return color;
        }

        public Stylers color(String color) {
            this.color = color;
            return this;
        }

        public Object lightness() {
            return lightness;
        }

        public Stylers lightness(Object lightness) {
            this.lightness = lightness;
            return this;
        }

        public String visibility() {
            return visibility;
        }

        public Stylers visibility(String visibility) {
            this.visibility = visibility;
            return this;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getVisibility() {
            return visibility;
        }

        public void setVisibility(String visibility) {
            this.visibility = visibility;
        }
    }
}
