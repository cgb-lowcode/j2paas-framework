/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts;

import cn.easyplatform.web.ext.ComponentBuilder;
import cn.easyplatform.web.ext.ComponentHandler;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.echarts.builder.EChartsBuilderFactory;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EChartsBuilder implements ComponentBuilder, Widget {

    private ComponentHandler dataHandler;

    private ECharts echarts;

    public EChartsBuilder(ComponentHandler dataHandler, ECharts echarts) {
        this.dataHandler = dataHandler;
        this.echarts = echarts;
    }

    @Override
    public Component build() {
        dataHandler.processEventHandler(echarts);
        echarts.setAttribute("$proxy", this);
        if (echarts.isImmediate())
            reload(echarts);
        return echarts;
    }

    @Override
    public void reload(Component widget) {
        echarts.setImmediate(true);
        EChartsBuilderFactory.createBuilder(echarts.getType()).build(echarts, dataHandler);
    }
}
