cardview.Cardview = zk.$extends(zul.Widget, {

    _img: null,
    _title: null,
    _author: null,
    _vardate: null,
    _portrait: null,
    _portraitflag: null,
    _header: null,
    _flagcontent:null,
    _flagcolour:null,
    _contenturl: null,
    _contentevt: null,
    _fronturl: null,
    _frontevt: null,
    _centreurl: null,
    _centreevt: null,
    _tailurl: null,
    _tailevt: null,
    _iconstyle: null,
    _fronttext: null,
    _centretext: null,
    _tailtext: null,
    _linkstyle: null,
    _sequence: null,
    _contenta: null,
    _contentb: null,
    _contentc: null,
    _contentd: null,
    _contente: null,
    _msgDate: null,
    _reloadFlag: null,
    _msgStatus: null,
    _temp:1,
    _moretxt:null,
    _scrollFlag:true,

    _query: null,
    _dbId: null,
    _filter: null,



	
	$define: {
        img: function(val) {
        },
        title: function(val) {
        },
        author: function(val) {
        },
        vardate: function(val) {
        },
        portrait: function(val) {
        },
        portraitflag: function(val) {
        },
        fronturl: function(val) {
        },
        frontevt: function(val) {
        },
        centreurl: function(val) {
        },
        centreevt: function(val) {
        },
        tailurl: function(val) {
        },
        tailevt: function(val) {
        },
        query: function(val) {
        },
        dbId: function(val) {
        },
        filter: function(val) {
        },
        header: function(val) {
        },
        flagcontent: function(val) {
        },
        flagcolour: function(val) {
        },
        iconstyle: function(val) {
        },
        fronttext: function(val) {
        },
        centretext: function(val) {
        },
        tailtext: function(val) {
        },
        linkstyle: function(val) {
        },
        sequence: function(val) {
        },
        contenta: function(val) {
            if(val.length>10){
                this._temp=10;
                this._moretxt="加载中...";
            }else{
                this._temp=val.length;
                this._moretxt="没有更多了";
            }
        },
        contentb: function(val) {
        },
        contentc: function(val) {
        },
        contentd: function(val) {
        },
        contente: function(val) {
        },
        contenturl: function(val) {
        },
        contentevt: function(val) {
        },
        msgDate: function(val) {
        },
        msgStatus: function(val) {
        },
        reloadFlag: function(val) {
            if(val==true){
                this.rerender();
                this._reloadFlag=false;
            }
        },
	},

	bind_: function () {
        var _self=this;
        $(window).scroll(function(){
            if(_self._scrollFlag){
                _self._scrollFlag=false;
                _self.scrollWindow();
            }
        });
		this.$supers(cardview.Cardview,'bind_', arguments);
	},


	unbind_: function () {
        $(window).unbind('scroll');
        this.$supers(cardview.Cardview,'unbind_', arguments);
	},

	doClick_: function (evt) {
	},

    doFrontLink: function (evt) {
        this.fire("onFrontLink",{data:evt});
    },

    doCentreLink: function (evt) {
        this.fire("onCentreLink",{data:evt});
    },

    doTailLink: function (evt) {
        this.fire("onTailLink",{data:evt});
    },

    doTouch: function (evt) {
        this.fire("onTouch",{str:evt});
    },

    allClear: function () {
        $("#"+this.uuid+"").innerHTML = "";
    },

    _init: function () {
    },

/*    redraw: function (out) {
       out.push('<div ', this.domAttrs_(), ' id="', this.uuid, '" />');
    },
*/

    console:function (str) {
        console.log(str);
    },


    scrollWindow:function() {
        var scrollTop = $(document).scrollTop();    //滚动条距离顶部的高度
        var scrollHeight = $(document).height();   //当前页面的总高度
        var clientHeight = $(window).height();    //当前可视的页面高度
        if (scrollTop >= scrollHeight - clientHeight) {   //距离顶部 >= 文档总高度-当前高度 即代表滑动到底部
            //滚动条到达底部
            var _self=this;
            if(_self._contenta.length>_self._temp ){
                if(_self._contenta.length-_self._temp>10){
                    _self._temp+=10;
                    _self._moretxt="加载中...";
                }else{
                    _self._temp+=_self._contenta.length-_self._temp;
                    _self._moretxt="没有更多了";
                }
                setTimeout(function(){
                    _self.rerender();
                },1000);
                setTimeout(function(){
                    _self._scrollFlag=true;//防止多次触发
                },1700);
            }
        } else if (scrollTop <= 0) {
            //到顶
        }else{
            this._scrollFlag=true;
        }
    },

});


