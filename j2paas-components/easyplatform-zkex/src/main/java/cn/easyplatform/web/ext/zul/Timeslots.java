/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.web.ext.Reloadable;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.ZkExt;
import org.zkoss.zul.Grid;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Timeslots extends Grid implements ZkExt, Reloadable {

    /**
     *
     */
    private static final long serialVersionUID = 1667150954794386870L;

    private String _query;

    private String _dbId;

    private String _filter;

    private int _beginTime = 0;

    private int _endTime = 24;

    private int _slots = 1;

    private String _itemStyle;

    /**
     * 是否必需重新加载
     */
    private boolean _force;

    @Override
    public boolean isForce() {
        return _force;
    }

    public void setForce(boolean force) {
        this._force = force;
    }

    /**
     * @return the itemStyle
     */
    public String getItemStyle() {
        return _itemStyle;
    }

    /**
     * @param itemStyle the itemStyle to set
     */
    public void setItemStyle(String itemStyle) {
        this._itemStyle = itemStyle;
    }

    /**
     * @return the query
     */
    public String getQuery() {
        return _query;
    }

    /**
     * @param query the query to set
     */
    public void setQuery(String query) {
        this._query = query;
    }

    /**
     * @return the dbId
     */
    public String getDbId() {
        return _dbId;
    }

    /**
     * @param dbId the dbId to set
     */
    public void setDbId(String dbId) {
        this._dbId = dbId;
    }

    /**
     * @return the filter
     */
    public String getFilter() {
        return _filter;
    }

    /**
     * @param filter the filter to set
     */
    public void setFilter(String filter) {
        this._filter = filter;
    }

    /**
     * @return the beginTime
     */
    public int getBeginTime() {
        return _beginTime;
    }

    /**
     * @param beginTime the beginTime to set
     */
    public void setBeginTime(int beginTime) {
        this._beginTime = beginTime;
    }

    /**
     * @return the endTime
     */
    public int getEndTime() {
        return _endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(int endTime) {
        this._endTime = endTime;
    }

    /**
     * @return the slots
     */
    public int getSlots() {
        return _slots;
    }

    /**
     * @param slots the slots to set
     */
    public void setSlots(int slots) {
        this._slots = slots;
    }

    @Override
    public void reload() {
        Widget ext = (Widget) getAttribute("$proxy");
        ext.reload(this);
    }

}
