/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext;

import org.zkoss.zk.ui.Component;

import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface PageContext {

    /**
     * 执行数据库查询
     *
     * @param query
     * @return
     */
    <T> List<T> selectList(Class<T> clazz, String query, Object... params);

    /**
     * 根据数据源执行数据库查询
     *
     * @param dsId
     * @param query
     * @return
     */
    <T> List<T> selectList0(Class<T> clazz, String dsId, String query, Object... params);

    /**
     * 分页查询
     *
     * @param query
     * @param pageNo
     * @param pageSize
     * @param getTotalCount
     * @return
     */
    <T> List<T> selectList(Class<T> clazz, String query, int pageNo, int pageSize, boolean getTotalCount, Object... params);

    /**
     * 分页查询
     *
     * @param dsId
     * @param query
     * @param pageNo
     * @param pageSize
     * @param getTotalCount
     * @return
     */
    <T> List<T> selectList(Class<T> clazz, String dsId, String query, int pageNo, int pageSize, boolean getTotalCount, Object... params);

    /**
     * 查询单笔数据
     *
     * @param clazz
     * @param query
     * @param params
     * @param <T>
     * @return
     */
    <T> T selectOne(Class<T> clazz, String query, Object... params);

    /**
     * 查询单笔数据
     *
     * @param clazz
     * @param query
     * @param params
     * @param <T>
     * @return
     */
    <T> T selectOne0(Class<T> clazz, String dsId, String query, Object... params);

    /**
     * 查询单个栏位
     *
     * @param query
     * @param params
     * @return
     */
    Object selectObject(String query, Object... params);

    /**
     * 查询单个栏位
     *
     * @param query
     * @param params
     * @return
     */
    Object selectObject0(String dsId, String query, Object... params);

    /**
     * 更新数据库
     *
     * @param sql
     * @param params
     * @return
     */
    int executeUpdate(String sql, Object... params);

    /**
     * 更新数据库
     *
     * @param sql
     * @param params
     * @return
     */
    int executeUpdate0(String dsId, String sql, Object... params);

    /**
     * 设置变量值
     *
     * @param name
     * @param value
     */
    void setVariable(String name, Object value);

    /**
     * 设置变量值
     *
     * @param names
     * @param values
     */
    void setVariables(String[] names, Object[] values);

    /**
     * 获取当前功能变量或栏位信息
     *
     * @param name
     * @return
     */
    Object getVariable(String name);

    /**
     * 获取当前功能变量或栏位信息
     *
     * @param names
     * @return
     */
    Object[] getVariables(String names[]);

    /**
     * 执行逻辑
     *
     * @param expr
     * @param names
     * @param values
     * @return
     */
    Object evalLogic(String expr, String[] names, Object[] values);

    /**
     * 执行逻辑
     *
     * @param expr
     * @param comps
     * @return
     */
    Object evalLogic(String expr, Component... comps);

    /**
     * 返回用户id
     *
     * @return
     */
    String getUserId();

    /**
     * 返回机构id
     *
     * @return
     */
    String getOrgId();

    /**
     * 返回项目id
     *
     * @return
     */
    String getProjectId();
}
