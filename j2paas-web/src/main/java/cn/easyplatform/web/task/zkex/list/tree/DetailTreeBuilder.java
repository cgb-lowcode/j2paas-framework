/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.list.tree;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.datalist.ListRecurVo;
import cn.easyplatform.web.ext.zul.Datalist;
import cn.easyplatform.web.task.OperableHandler;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.North;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DetailTreeBuilder extends AbstractOperableTreeBuilder {

    /**
     * @param main
     * @param dl
     */
    public DetailTreeBuilder(OperableHandler main, Datalist dl, Component anchor) {
        super(main, dl, anchor);
    }

    protected Component createContent() {
        Borderlayout layout = new Borderlayout();
        if (getEntity().isShowPanel() && !Strings.isBlank(this.layout.getPanel())) {
            North north = new North();
            north.setHflex("1");
            north.setVflex("1");
            north.setBorder("none");
            if (this.layout instanceof ListRecurVo)
                north.appendChild(buildPanel(this.layout.getPanel(),
                        ((ListRecurVo) this.layout).getPanelQueryInfo()));
            else
                north.appendChild(buildPanel(this.layout.getPanel(), null));
            layout.appendChild(north);
        }
        if (getEntity().getPageSize() > 0 && getEntity().isShowPaging()) {
            listExt.setMold("paging");
            listExt.setPageSize(getEntity().getPageSize());
            listExt.getPaginal().setPageSize(listExt.getPageSize());
            if (this.layout instanceof ListRecurVo) {
                int activePage = ((ListRecurVo) this.layout).getStartPageNo();
                if (activePage > 0)
                    listExt.getPagingChild().setActivePage(activePage - 1);
            }
            paging = listExt.getPagingChild();
            paging.setAutohide(getEntity().isPagingAutohide());
            paging.setDetailed(getEntity().isPagingDetailed());
            if (getEntity().getPagingMold() != null)
                paging.setMold(getEntity().getPagingMold());
            if (getEntity().getPagingStyle() != null)
                paging.setStyle(getEntity().getPagingStyle());
        }
        Center center = new Center();
        center.setHflex("1");
        center.setVflex("1");
        center.setBorder("none");
        layout.appendChild(center);
        if (!Strings.isBlank(listExt.getHeight()))
            layout.setHeight(listExt.getHeight());
        else if (!Strings.isBlank(listExt.getVflex()))
            layout.setVflex(listExt.getVflex());
        else
            layout.setVflex("1");
        if (!Strings.isBlank(listExt.getWidth()))
            layout.setWidth(listExt.getWidth());
        else if (!Strings.isBlank(listExt.getHflex()))
            layout.setHflex(listExt.getHflex());
        else
            layout.setHflex("1");
        if (Strings.isBlank(listExt.getWidth()))
            listExt.setHflex("1");
        if (Strings.isBlank(listExt.getHeight()))
            listExt.setVflex("1");
        Components.replace(getEntity(), layout);
        listExt.setParent(center);
        return layout;
    }

    @Override
    public void paging(int pageNo) {
    }

    @Override
    protected void setPagingInfo(int totalSize, int activePageNo) {
        if (listExt.getPagingChild() != null) {
            //listExt.getPagingChild().setTotalSize(totalSize);
            listExt.getPagingChild().setActivePage(activePageNo);
        }
    }
}
