/**
 * Copyright 2019 吉鼎科技.
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.listener;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.ListSelectionRequestMessage;
import cn.easyplatform.messages.vos.datalist.ListSelectionVo;
import cn.easyplatform.spi.service.ListService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.dialog.MessageBox;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.task.event.EventListenerHandler;
import cn.easyplatform.web.task.zkex.list.OperableListSupport;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treeitem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SelectedEventListener implements EventListener<Event> {

    private OperableListSupport os;

    public SelectedEventListener(OperableListSupport os) {
        this.os = os;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onEvent(Event evt) throws Exception {
        List<Object[]> keys = new ArrayList<Object[]>();
        if (os.getComponent() instanceof Listbox) {
            for (Listitem li : ((Listbox) os.getComponent()).getSelectedItems()) {
                ListRowVo rv = li.getValue();
                if (os.isCustom())
                    keys.add(rv.getData());
                else
                    keys.add(rv.getKeys());
            }
        } else {
            SelectEvent<Treeitem, Set<Treeitem>> event = (SelectEvent<Treeitem, Set<Treeitem>>) evt;
            if (os.getEntity().isCheckall()
                    && event.getReference().isContainer()) {
                Iterator<Component> itr = event.getReference()
                        .queryAll("treeitem").iterator();
                while (itr.hasNext()) {
                    Treeitem ti = (Treeitem) itr.next();
                    ti.setCheckable(event.getReference().isCheckable());
                    ti.setSelected(event.getReference().isSelected());
                }
            }
            Set<Treeitem> selectedItems = ((Tree) os.getComponent())
                    .getSelectedItems();
            for (Treeitem li : selectedItems) {
                if (li.getValue() != null) {
                    ListRowVo rv = li.getValue();
                    if (os.isCustom())
                        keys.add(rv.getData());
                    else
                        keys.add(rv.getKeys());
                }
            }
        }
        // if (!keys.isEmpty()) {
        ListService dls = ServiceLocator
                .lookup(ListService.class);
        IResponseMessage<?> resp = dls
                .doSelection(new ListSelectionRequestMessage(os.getId(),
                        new ListSelectionVo(os.getComponent().getId(), keys)));
        if (!resp.isSuccess())
            MessageBox.showMessage(resp);
        else if (!Strings.isBlank(os.getLayout().getOnSelect()))//触发选择事件
            new EventListenerHandler(Events.ON_CHECK, os, os.getLayout().getOnSelect()).onEvent(evt);
        // }
    }
}
