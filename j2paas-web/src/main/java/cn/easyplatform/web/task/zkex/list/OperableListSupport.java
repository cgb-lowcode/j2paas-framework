/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.list;

import cn.easyplatform.messages.vos.datalist.ListUpdatableRowVo;
import cn.easyplatform.web.task.event.EventEntry;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.task.OperableHandler;
import cn.easyplatform.web.task.support.ManagedComponent;

import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface OperableListSupport extends OperableHandler, ManagedComponent,
        ListSupport {
    /**
     * 读取
     *
     * @param entry
     */
    void read(EventEntry<String> entry);

    /**
     * 列表是否可编辑
     *
     * @param name
     * @return
     */
    boolean isEditable(String name);

    /**
     * 更新
     *
     * @param lor
     */
    void update(ListUpdatableRowVo lor);

    /**
     * 增加子列表
     *
     * @param name
     */
    void addChild(String name);

    /**
     * @return
     */
    Object[] getCurrentFromKeys();

    /**
     * 列表的所有子列表
     *
     * @return
     */
    List<String> getChildren();

    /**
     * 重置
     *
     * @param clearAll
     */
    void reset(boolean clearAll);

    /**
     * 获取原始记录，ListRowVo或FieldVo[]
     *
     * @param orign
     * @return
     */
    <T> List<T> getData(boolean sel, boolean orign);

    /**
     * 获取记录主键
     *
     * @param sel
     * @return
     */
    List<Object[]> getKeys(boolean sel);

    /**
     * 使指定的列可否显示
     *
     * @param keyValue
     * @param visible
     * @return
     */
    boolean setRowVisible(Object[] keyValue, boolean visible);

    /**
     * 设置记录项
     *
     * @param keyValue
     * @param action   0表示删除,1表示隐藏，2表示显示
     * @return
     */
    boolean doItem(Object[] keyValue, int action);

    /**
     * 重新统计
     */
    void refreshFoot();

    void first();

    void last();

    void previous();

    void next();
}
