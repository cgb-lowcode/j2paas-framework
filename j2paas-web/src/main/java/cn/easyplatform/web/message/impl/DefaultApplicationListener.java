/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.message.impl;

import cn.easyplatform.spi.listener.ApplicationListener;
import cn.easyplatform.spi.listener.event.AppEvent;
import cn.easyplatform.spi.listener.event.Event;
import cn.easyplatform.web.WebApps;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DefaultApplicationListener implements ApplicationListener {

    @Override
    public void publish(String projectId, String topic, Serializable msg, String... toUsers) {
        if (!(msg instanceof Event))
            msg = new AppEvent(topic, msg);
        WebApps.me().publish(projectId, toUsers, (Event) msg);
    }

    @Override
    public void send(String projectId, String topic, Serializable msg, String... toUsers) {

    }

    @Override
    public void close() {
    }
}
