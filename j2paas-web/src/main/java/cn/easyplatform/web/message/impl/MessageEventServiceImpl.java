/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.message.impl;

import cn.easyplatform.spi.listener.event.Event;
import cn.easyplatform.web.message.MessageEventFactory;
import cn.easyplatform.web.message.MessageEventService;
import cn.easyplatform.web.message.entity.Message;
import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

import java.util.concurrent.Executors;

/**
 * 系统内置应用侦听者
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MessageEventServiceImpl implements MessageEventService {

    private Disruptor<Message> disruptor;

    private RingBuffer<Message> ringBuffer;

    @Override
    public void start(EventHandler<Message> eh) {
        disruptor = new Disruptor<>(new MessageEventFactory(), 1024 * 1024, Executors.defaultThreadFactory(), ProducerType.SINGLE, new BlockingWaitStrategy());
        disruptor.handleEventsWith(eh);
        disruptor.start();
        ringBuffer = disruptor.getRingBuffer();
    }

    @Override
    public void onEvent(String projectId, String[] target, Event event) {
        long sequence = ringBuffer.next();
        try {
            Message msg = ringBuffer.get(sequence);
            msg.setProjectId(projectId);
            msg.setTarget(target);
            msg.setEntity(event);
        } finally {
            ringBuffer.publish(sequence);
        }
    }

    @Override
    public void stop() {
        if (disruptor != null)
            disruptor.shutdown();
    }
}
