/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.layout.menu;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.AuthorizationVo;
import cn.easyplatform.web.utils.ExtUtils;
import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TabboxMenuBuilder extends AbstractMenuBuilder<Tabbox> {

    public TabboxMenuBuilder(Tabbox tabbox) {
        super(tabbox);
    }

    @Override
    public void fill(AuthorizationVo av) {
        super.fill(av);
        box.addEventListener(Events.ON_SELECT, this);
    }

    protected void createMenu(Menu menu) {
        Tab tab = new Tab();
        tab.setLabel(menu.getMenu().getName());
        if (!Strings.isBlank(menu.getMenu().getImage()))
            PageUtils.setTaskIcon(tab, menu.getMenu().getImage());
        else
            tab.setIconSclass(ExtUtils.getIconSclass());
        tab.setValue(menu);
        tab.setParent(box.getTabs());
    }

    @Override
    public void onEvent(Event event) throws Exception {
        Tab tab = (Tab) event.getTarget();
        if (tab.getLinkedPanel() == null) {
            Menu mv = tab.getValue();
            Tabpanel tabpanel = new Tabpanel();
            tabpanel.setParent(box.getTabpanels());
            Borderlayout layout = new Borderlayout();
            layout.setParent(tabpanel);
            createPanel(layout, mv);
        }
    }
}
